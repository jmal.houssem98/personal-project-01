package com.hcm.gestiondestock.services.strategy;

import com.flickr4java.flickr.FlickrException;
import com.hcm.gestiondestock.dto.UtilisateurDto;
import com.hcm.gestiondestock.exception.ErrorCodes;
import com.hcm.gestiondestock.exception.InvalidOperationException;
import com.hcm.gestiondestock.services.FlickrService;
import com.hcm.gestiondestock.services.UtilisateurService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.ExecutionException;


@Service("utilisateurStrategy")
@Slf4j
public class SaveUtilisateurPhoto implements Strategy<UtilisateurDto> {
    private FlickrService flickrService;
    private UtilisateurService utilisateurService;
    @Autowired
    public SaveUtilisateurPhoto(FlickrService flickrService, UtilisateurService utilisateurService) {
        this.flickrService = flickrService;
        this.utilisateurService = utilisateurService;
    }
    @Override
    public UtilisateurDto savePhoto(Integer id, InputStream photo, String titre) throws FlickrException, IOException, ExecutionException, InterruptedException {
        UtilisateurDto utilisateur = utilisateurService.findById(id);
        String urlPhoto = flickrService.savePhoto(photo, titre);
        if (!StringUtils.hasLength(urlPhoto)) {
            throw new InvalidOperationException("Erreur lors de l'enregistrement de photo de l'utilisateur", ErrorCodes.UPDATE_PHOTO_EXCEPTION);
        }
        utilisateur.setPhoto(urlPhoto);
        return utilisateurService.save(utilisateur);
    }
}
