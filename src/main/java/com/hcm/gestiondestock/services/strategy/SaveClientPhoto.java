package com.hcm.gestiondestock.services.strategy;

import com.flickr4java.flickr.FlickrException;
import com.hcm.gestiondestock.dto.ClientDto;
import com.hcm.gestiondestock.exception.ErrorCodes;
import com.hcm.gestiondestock.exception.InvalidOperationException;
import com.hcm.gestiondestock.services.ClientService;
import com.hcm.gestiondestock.services.FlickrService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.ExecutionException;

@Service("clientStrategy")
@Slf4j
public class
SaveClientPhoto implements Strategy<ClientDto> {

    private FlickrService flickrService;
    private ClientService clientService;
    @Autowired
    public SaveClientPhoto(FlickrService flickrService, ClientService clientService) {
        this.flickrService = flickrService;
        this.clientService = clientService;
    }
    @Override
    public ClientDto savePhoto(Integer id, InputStream photo, String titre) throws FlickrException, IOException, ExecutionException, InterruptedException {
        ClientDto client = clientService.findById(id);
        String urlPhoto = flickrService.savePhoto(photo, titre);
        if (!StringUtils.hasLength(urlPhoto)) {
            throw new InvalidOperationException("Erreur lors de l'enregistrement de photo du client", ErrorCodes.UPDATE_PHOTO_EXCEPTION);
        }
        client.setPhoto(urlPhoto);
        return clientService.save(client);
    }
}
