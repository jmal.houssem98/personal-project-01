package com.hcm.gestiondestock.services;

import com.hcm.gestiondestock.dto.ArticleDto;
import com.hcm.gestiondestock.dto.LigneCommandeClientDto;
import com.hcm.gestiondestock.dto.LigneCommandeFournisseurDto;
import com.hcm.gestiondestock.dto.LigneVenteDto;
import com.hcm.gestiondestock.repository.CategoryRepository;

import java.util.List;

public interface ArticleService {

    ArticleDto save(ArticleDto dto);

    ArticleDto findById(Integer id);

    ArticleDto findByCodeArticle(String codeArticle);

    List<ArticleDto> findAll();

    List<LigneVenteDto> findHistoriqueVentes(Integer idArticle);

    List<LigneCommandeClientDto> findHistoriqueCommandeClient(Integer idArticle);

    List<LigneCommandeFournisseurDto> findHistoriqueCommandeFournisseur(Integer idArticle);

    List<ArticleDto> findAllArticleByIdCategory(Integer idCategory);

    void delete(Integer id);
}
