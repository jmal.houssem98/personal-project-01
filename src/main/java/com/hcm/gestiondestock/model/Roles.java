package com.hcm.gestiondestock.model;


import jakarta.persistence.*;
import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "roles")
public class Roles  extends AbstractEntity{

    @Column(name = "rolename")
    public String roleName;

    @ManyToOne
    @JoinColumn(name = "idutilsateur")
    private Utilisateur utilisateur;
}
