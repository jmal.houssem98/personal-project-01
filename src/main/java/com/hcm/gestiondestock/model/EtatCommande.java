package com.hcm.gestiondestock.model;

public enum EtatCommande {
    EN_PREPARATION,
    VALIDEE,
    LIVREE
}
