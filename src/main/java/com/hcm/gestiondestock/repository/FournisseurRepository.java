package com.hcm.gestiondestock.repository;

import com.hcm.gestiondestock.model.Fournisseur;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FournisseurRepository extends JpaRepository< Fournisseur, Integer> {
}
