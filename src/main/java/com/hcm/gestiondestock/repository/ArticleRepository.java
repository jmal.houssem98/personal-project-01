package com.hcm.gestiondestock.repository;

import com.hcm.gestiondestock.model.Article;
import io.swagger.v3.oas.annotations.Hidden;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;


public interface ArticleRepository extends JpaRepository< Article, Integer> {
    Optional<Article> findArticleByCodeArticle(String codeArticle);
    List<Article>findAllByCategoryId(Integer IdCategory);
}
