package com.hcm.gestiondestock.repository;

import com.hcm.gestiondestock.model.Entreprise;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EntrepriseRepository extends JpaRepository< Entreprise, Integer> {

}
