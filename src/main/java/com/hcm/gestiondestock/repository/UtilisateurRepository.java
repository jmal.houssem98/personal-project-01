package com.hcm.gestiondestock.repository;

import com.hcm.gestiondestock.model.Utilisateur;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UtilisateurRepository extends JpaRepository< Utilisateur,Integer> {
}
