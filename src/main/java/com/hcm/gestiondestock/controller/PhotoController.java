package com.hcm.gestiondestock.controller;

import com.flickr4java.flickr.FlickrException;
import com.hcm.gestiondestock.controller.api.PhotoApi;
import com.hcm.gestiondestock.services.strategy.StrategyPhotoContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.concurrent.ExecutionException;


@RestController
public class PhotoController implements PhotoApi {

    private StrategyPhotoContext strategyPhotoContext;

    @Autowired
    public PhotoController(StrategyPhotoContext strategyPhotoContext) {
        this.strategyPhotoContext = strategyPhotoContext;
    }

    @Override
    public Object savePhoto(String context, Integer id, MultipartFile photo, String title) throws IOException, FlickrException, ExecutionException, InterruptedException {
        return strategyPhotoContext.savePhoto(context, id, photo.getInputStream(), title);
    }
}
