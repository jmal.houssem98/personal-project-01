package com.hcm.gestiondestock.controller.api;

import com.hcm.gestiondestock.dto.ArticleDto;
import com.hcm.gestiondestock.dto.LigneCommandeClientDto;
import com.hcm.gestiondestock.dto.LigneCommandeFournisseurDto;
import com.hcm.gestiondestock.dto.LigneVenteDto;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static com.hcm.gestiondestock.utils.Constants.APP_ROOT;

@Tag(name = "Articles")
public interface ArticleApi {
    @PostMapping(value = APP_ROOT+"/articles/create", consumes = MediaType.APPLICATION_JSON_VALUE, produces=MediaType.APPLICATION_JSON_VALUE)
    @Operation(tags = "Enregistrer un article", description = "Cette methode permet d'enregistrer ou modifier un article")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description  = "L'objet article cree / modifie",
                    content = {@Content(mediaType = "application/json", schema = @Schema(implementation = ArticleDto.class)) }),
            @ApiResponse(responseCode = "400", description  = "L'objet article n'est pas valide")
    })
    ArticleDto save(@RequestBody ArticleDto dto);

    @GetMapping(value= APP_ROOT+"/articles/{idArticle}",produces=MediaType.APPLICATION_JSON_VALUE)
    ArticleDto findById(@PathVariable("idArticle") Integer idArticle);

    @GetMapping(value= APP_ROOT+"/articles/filter/{codeArticle}",produces=MediaType.APPLICATION_JSON_VALUE)
    ArticleDto findByCodeArticle(@PathVariable("codeArticle") String codeArticle);

    @GetMapping(value= APP_ROOT+"/articles/all",produces=MediaType.APPLICATION_JSON_VALUE)
    List<ArticleDto> findAll();

    @GetMapping(value= APP_ROOT+"/articles/historique/vente/{idArticle}",produces=MediaType.APPLICATION_JSON_VALUE)
    List<LigneVenteDto> findHistoriqueVentes(@PathVariable("idArticle") Integer idArticle);

    @GetMapping(value= APP_ROOT+"/articles/historique/client/{idArticle}",produces=MediaType.APPLICATION_JSON_VALUE)
    List<LigneCommandeClientDto> findHistoriqueCommandeClient(@PathVariable("idArticle") Integer idArticle);

    @GetMapping(value = APP_ROOT + "/articles/historique/commandefournisseur/{idArticle}", produces = MediaType.APPLICATION_JSON_VALUE)
    List<LigneCommandeFournisseurDto> findHistoriqueCommandeFournisseur(@PathVariable("idArticle") Integer idArticle);

    @GetMapping(value = APP_ROOT + "/articles/filter/category/{idCategory}", produces = MediaType.APPLICATION_JSON_VALUE)
    List<ArticleDto> findAllArticleByIdCategory(@PathVariable("idCategory") Integer idCategory);

    @DeleteMapping(value=APP_ROOT+"/articles/delete/{idArticle}")
    void delete(@PathVariable("idArticle") Integer id);
}
